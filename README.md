# Synack LaunchPoint Guard

I had a few issues with Synack's LaunchPoint Guard (lp-guard) script on Kali 2020.1, so I made some modifications to it.

Primarily, my issues were as follows:
* I wanted to use the built-in NetworkManager OpenVPN client rather than `openvpn --config ...`
* I wanted to resolve the issue with NetworkManager LaunchPoint VPN not having the correct routes by default
* I wanted LaunchPoint to automatically connect when lp-guard was run
* I wanted LaunchPoint to automatically disconnect when `lp-guard -down` was run
* I wanted logs to go to `/var/log/`

## Installation

You should be able to run the following command to install it:

```
sudo wget https://gitlab.com/bamhm182/synack-launchpoint-guard/-/raw/master/lp-guard-debian.sh -O /usr/local/bin/lp-guard && sudo chmod +x /usr/local/bin/lp-guard
```

## Testing

**NOTE: This works well for me. PLEASE do not take my word for it. Make sure you know what the script is doing and how. I will not be held responsible if you use this, it doesn't work on your machine, and you get in trouble for testing while not on LaunchPoint.**

This script does the following when run as `lp-guard`:
1. Modifies your routes to force all traffic to go to 127.0.0.1 (kill it) except it is pointing at a Synack LaunchPoint server. Those still go through the default gateway
1. Configures NetworkManager VPN to force all traffic through the VPN if it is up
1. Starts NetworkManager LaunchPoint VPN

This script does the following when run as `lp-guard -down`
1. Stops NetworkManager LaunchPoint VPN
1. Removes routes forcing all traffic to go to 127.0.0.1 and those allowing LaunchPoint server connections through the default gateway

An example of one way you could test it is to:
1. Run `lp-guard` and ensure you can access `https://login.synack.com` and only `https://login.synack.com`
1. Login and connect to a Target
1. Ensure you can connect to external websites, but do not start testing the Target
1. Kill the LaunchPoint VPN
1. Ensure you cannot connect to external websites, especially those of the Target
1. Reconnect to the LaunchPoint VPN
1. Ensure you can connect to the LaunchPoint VPN
