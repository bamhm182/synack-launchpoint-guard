#!/bin/bash
# Use this script to avoid sending accidental traffic to clients when disconnected from LP

main () {
	if [ "$#" -eq 0 ]; then
	  setup
	  modify_vpn_config
	  add_routes
	  sudo -u ${SUDO_USER} nmcli connection up ${lp_vpn}
	elif [ "$#" -gt 0 -a "$1" == "-down" ]; then
	  setup $1
	  delete_routes
	else
	  usage
	fi
}

# main() Functions

setup () {
  determine_lp_vpn

  if [ "${1}" == "-down" ]; then
    sudo -u ${SUDO_USER} nmcli connection down ${lp_vpn}
  fi

  check_for_root
  check_vpn

  nameservers=($(awk '/nameserver/{print $2}' /etc/resolv.conf))
  lp_ips=($(dig +short production-lp-vpn.synack-lp.com))
  default_gws=($(ip route show table main | awk '{print $3}'))
  default_gw=$(ip route | awk '/^default via/{print $3}')
  lp_log=/var/log/lp-guard.log

  date >> ${lp_log} 2>&1
  ip route show table main >> ${lp_log} 2>&1
}

modify_vpn_config () {
  if [ -z "$(nmcli -f ipv4.routes connection show ${lp_vpn} | grep '0.0.0.0/1')" ]; then
	  echo "Adding static route for 0.0.0.0/1 through VPN..."
	  nmcli connection modify ${lp_vpn} +ipv4.routes "0.0.0.0/1"
  fi

  if [ -z "$(nmcli -f ipv4.routes connection show ${lp_vpn} | grep '128.0.0.0/1')" ]; then
	  echo "Adding static route for 128.0.0.0/1 through VPN..."
	  nmcli connection modify ${lp_vpn} +ipv4.routes "128.0.0.0/1"
  fi

  if [ -z "$(nmcli -f vpn.persistent connection show ${lp_vpn} | grep 'yes')" ]; then
      echo "Setting VPN Persistance..."
      nmcli connection modify ${lp_vpn} vpn.persistent yes
  fi
}

add_routes () {
  {
    set -x
    add_main_routes
    add_lp_routes
    set +x
  } >> ${lp_log} 2>&1
  check_success
}

delete_routes () {
  {
    set -x
    delete_main_routes
    delete_lp_routes
    set +x
  } >> ${lp_log} 2>&1
  echo "LP Route cleanup finished"
}

usage () {
	echo "Usage: sudo $(basename $0) -[down|help]"
}

# setup() Functions

determine_lp_vpn () {
	for item in $(nmcli -t connection | awk -F: '{ print $2 }'); do
		if [ ! -z "$(nmcli -f vpn.data connection show ${item} | grep 'synack-lp.com')" ]; then
			lp_vpn=${item}
		fi
	done
}

check_for_root () {
  if [ "$EUID" -ne 0 ]; then
    echo "Please run as root"
    usage
    exit 1
  fi
}

check_vpn () {
  if [ ! -z "$(nmcli -f GENERAL.STATE connection show ${lp_vpn})" ]; then
	  echo "Please ensure you do not run this script while connected to the LaunchPoint VPN"
	  exit 1
  fi
}

# add_routes() Functions

add_main_routes () {
  ip route add 0.0.0.0/1 via 127.0.0.1 metric 900
  ip route add 128.0.0.0/1 via 127.0.0.1 metric 900
}


add_lp_routes () {
  for ip in "${nameservers[@]}"; do
    ip route add $ip via $default_gw metric 900
  done

  for ip in "${lp_ips[@]}"; do
    ip route add $ip via $default_gw metric 900
  done
}

check_success () {
  for ip in "${lp_ips[@]}"; do
    if ! ip route | grep -q "$ip via $default_gw"; then
      delete_routes >> ${lp_log} 2>&1
      echo "Failed to setup route." | tee ${lp_log} 2>&1
      exit 1
    fi
  done
  echo "Ready to connect to LP"
}

# delete_routes() Functions

delete_main_routes () {
  ip route delete 0.0.0.0/1 via 127.0.0.1 metric 900
  ip route delete 128.0.0.0/1 via 127.0.0.1 metric 900
}

delete_lp_routes () {
  for ip in "${lp_ips[@]}"; do
	  entry=ip route | awk "/^${ip} via /"
	  ip route delete $(ip route | awk "/^${ip} via /") 
  done
}

main $@
